package vikrela.com.dto;

/**
 * Created by Harsh on 6/30/2017.
 */
public class CommodityDTO {
    String commodityName;
    String commodityMinRate;
    String commodityMaxRate;
    String commodityModalPrice;
    String market;

    public String getCommodityName() {
        return commodityName;
    }

    public void setCommodityName(String commodityName) {
        this.commodityName = commodityName;
    }

    public String getCommodityMinRate() {
        return commodityMinRate;
    }

    public void setCommodityMinRate(String commodityMinRate) {
        this.commodityMinRate = commodityMinRate;
    }

    public String getCommodityMaxRate() {
        return commodityMaxRate;
    }

    public void setCommodityMaxRate(String commodityMaxRate) {
        this.commodityMaxRate = commodityMaxRate;
    }

    public String getCommodityModalPrice() {
        return commodityModalPrice;
    }

    public void setCommodityModalPrice(String commodityModalPrice) {
        this.commodityModalPrice = commodityModalPrice;
    }

    public String getMarket() {
        return market;
    }

    public void setMarket(String market) {
        this.market = market;
    }
}
