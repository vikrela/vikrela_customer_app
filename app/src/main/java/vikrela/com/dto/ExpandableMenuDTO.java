package vikrela.com.dto;

/**
 * Created by Harsh on 6/15/2017.
 */
public class ExpandableMenuDTO {

    String name;
    int icon;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getIcon() {
        return icon;
    }

    public void setIcon(int icon) {
        this.icon = icon;
    }
}
