package vikrela.com.constant;

/**
 * Created by Harsh on 6/17/2017.
 */
public interface NetworkConstants {
    public String NETWORK_IP = "http://kkwagheducationsocietyalumni.in/Vikrela/Android/";
    public String INFORMATION_URL = NETWORK_IP + "/retriveHawkerData.php";
    public String INFORMATION_URL_ID = NETWORK_IP + "/retriveHawkerDataFromLisenceNo.php";
    public String COMPLAINT_URL = NETWORK_IP + "/Complaint.php";
    public String COMMODITY_URL = "https://data.gov.in/api/datastore/resource.json?resource_id=9ef84268-d588-465a-a308-a864a43d0070&api-key=280a9c101472ef23192f087107bfa26c&filters[district]=\"Nashik\"";
    public static final String DISTRICT_URL = "https://www.whizapi.com/api/v2/util/ui/in/indian-city-by-state?stateid=3&project-app-key=g24srjf38hvjnqz368gmnpkx";
}
