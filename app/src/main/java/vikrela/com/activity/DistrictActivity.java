package vikrela.com.activity;

import android.app.Activity;
import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListAdapter;
import android.widget.ListView;

import com.afollestad.materialdialogs.MaterialDialog;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import vikrela.com.R;
import vikrela.com.adapter.StateAdapter;
import vikrela.com.app.AppController;
import vikrela.com.constant.NetworkConstants;
import vikrela.com.dto.StateDTO;

public class DistrictActivity extends AppCompatActivity {

    @Bind(R.id.searchDistrict)
    android.widget.SearchView searchDistrict;
    @Bind(R.id.listDistrict)
    ListView listDistrict;
    List<StateDTO> stateDTOList;
    MaterialDialog dialog;
    StateAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_distrcit);
        populate();
    }

    private void populate() {
        ButterKnife.bind(this);
        stateDTOList = new ArrayList<>();
        dialog = new MaterialDialog.Builder(DistrictActivity.this)
                .title("Loading")
                .content("Please Wait...")
                .progress(true, 0)
                .show();
        StringRequest request = new StringRequest(Request.Method.GET, NetworkConstants.DISTRICT_URL, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                dialog.dismiss();
                Log.d("Response:::", response);
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    JSONArray jsonArray = jsonObject.getJSONArray("Data");
                    if (jsonArray.length() > 0){
                        for (int i = 0; i < jsonArray.length(); i++){
                            JSONObject jsonObject1 = jsonArray.getJSONObject(i);
                            StateDTO dto = new StateDTO();
                            dto.setName(jsonObject1.getString("city"));
                            stateDTOList.add(dto);
                        }
                        adapter = new StateAdapter(DistrictActivity.this, stateDTOList);
                        listDistrict.setAdapter(adapter);
                        listDistrict.setTextFilterEnabled(true);
                        adapter.notifyDataSetChanged();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        });
        AppController.getInstance().addToRequestQueue(request);

        SearchManager manager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        searchDistrict.setSearchableInfo(manager.getSearchableInfo(getComponentName()));
        searchDistrict.setIconifiedByDefault(false);
        searchDistrict.setQueryHint(getString(R.string.select_district));
        searchDistrict.setOnQueryTextListener(new android.widget.SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                adapter.filter(newText);
                return false;
            }
        });

        listDistrict.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent = new Intent();
                StateDTO dto = (StateDTO) parent.getAdapter().getItem(position);
                String district = dto.getName();
                intent.putExtra("district", district);
                setResult(Activity.RESULT_OK, intent);
                finish();
            }
        });
    }
}
