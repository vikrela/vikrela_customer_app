package vikrela.com.activity;

import android.content.Intent;
import android.location.Address;
import android.location.Geocoder;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.location.places.ui.PlaceAutocomplete;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import butterknife.Bind;
import butterknife.ButterKnife;
import vikrela.com.R;
import vikrela.com.adapter.CityAdapter;
import vikrela.com.app.GPSTracker;
import vikrela.com.dto.CityDTO;

/**
 * Created by Harsh on 6/14/2017.
 */
public class AreaActivity extends AppCompatActivity {

    @Bind(R.id.toolbar)
    Toolbar toolbar;
    @Bind(R.id.txtDetectLocation)
    TextView txtDetectLocation;
    @Bind(R.id.txtSearchCity)
    TextView txtSearchCity;
    @Bind(R.id.listPopularCities)
    RecyclerView listPopularCities;
    private CityAdapter adapter;
    private List<CityDTO> cityDTOList;
    int PLACE_AUTOCOMPLETE_REQUEST_CODE = 1;
    GPSTracker tracker;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_area);
        populate();
    }

    private void populate() {
        ButterKnife.bind(this);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Location");
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        tracker = new GPSTracker(AreaActivity.this);
        if (tracker.canGetLocation()){
            double lat = tracker.getLatitude();
            double lon = tracker.getLongitude();
        }
        txtDetectLocation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (tracker.canGetLocation()){
                    MaterialDialog dialog = new MaterialDialog.Builder(AreaActivity.this)
                            .title("")
                            .content("Please Wait")
                            .progress(false, 0).show();
                    double lat = tracker.getLatitude();
                    double lon = tracker.getLongitude();
                    Geocoder geocoder = new Geocoder(AreaActivity.this, Locale.getDefault());
                    try {
                        List<Address> addresses = geocoder.getFromLocation(lat, lon, 1);
                        String cityName = addresses.get(0).getAddressLine(0);
                        Toast.makeText(getApplicationContext(), cityName + " location is set", Toast.LENGTH_SHORT).show();
                        dialog.dismiss();
                        startActivity(new Intent(AreaActivity.this, DashboardActivity.class));
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                }
            }
        });
        adapter = new CityAdapter(AreaActivity.this, getData(), new CityAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(CityDTO cityDTO) {
                startActivity(new Intent(AreaActivity.this, DashboardActivity.class));
                Toast.makeText(getApplicationContext(), cityDTO.getName() + " location is set", Toast.LENGTH_SHORT).show();
            }
        });
        listPopularCities.setAdapter(adapter);
        adapter.notifyDataSetChanged();
        listPopularCities.setLayoutManager(new GridLayoutManager(this, 4));

        txtSearchCity.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    Intent intent = new PlaceAutocomplete.IntentBuilder(PlaceAutocomplete.MODE_FULLSCREEN).build(AreaActivity.this);
                    startActivityForResult(intent, PLACE_AUTOCOMPLETE_REQUEST_CODE);
                } catch (GooglePlayServicesRepairableException e) {
                    e.printStackTrace();
                } catch (GooglePlayServicesNotAvailableException e) {
                    e.printStackTrace();
                }
            }
        });

    }

    public List<CityDTO> getData() {
        cityDTOList = new ArrayList<>();
        String[] names = {"Nasik Road", "College Road", "Indiranagar", "Parab Nagar", "Panchwati", "Cidco",
                "Wadala Road", "Ravivaar Kharanja", "Shalimar", "Dwarka Chowk", "Gangapur Road", "Akashwani Kendra"};
        int[] icons = {R.drawable.paris, R.drawable.park, R.drawable.field, R.drawable.fountain, R.drawable.city, R.drawable.island,
        R.drawable.london, R.drawable.moscow, R.drawable.village, R.drawable.mountains, R.drawable.volcano, R.drawable.beach};
        for (int i = 0; i < names.length; i++) {
            CityDTO dto = new CityDTO();
            dto.setName(names[i]);
            dto.setIcon(icons[i]);
            cityDTOList.add(dto);
        }
        return cityDTOList;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case android.R.id.home:
                onBackPressed();
        }
        return super.onOptionsItemSelected(item);
    }
}
