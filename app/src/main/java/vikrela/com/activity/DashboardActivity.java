package vikrela.com.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.NavigationView;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;

import butterknife.Bind;
import butterknife.ButterKnife;
import vikrela.com.R;
import vikrela.com.app.GPSTracker;
import vikrela.com.fragment.DashboardFragment;

/**
 * Created by Harsh on 6/14/2017.
 */
public class DashboardActivity extends AppCompatActivity {

    @Bind(R.id.toolbar)
    Toolbar toolbar;
    @Bind(R.id.drawer_layout)
    DrawerLayout drawerLayout;
    @Bind(R.id.navigation_view)
    NavigationView navigationView;
    TextView txtProductInfo;
    GPSTracker tracker;
    private MenuItem previousMenuItem;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dashboard);
        populate();
    }

    private void populate() {
        ButterKnife.bind(this);
        tracker = new GPSTracker(DashboardActivity.this);
        if (tracker.canGetLocation()) {
            double lat = tracker.getLatitude();
            double lon = tracker.getLongitude();
        }
        setSupportActionBar(toolbar);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("");
        txtProductInfo = (TextView) toolbar.findViewById(R.id.txtProductInfo);
        txtProductInfo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                View view = getLayoutInflater().inflate(R.layout.view_product, null);
                final FrameLayout linearLayout = (FrameLayout) findViewById(R.id.frameMenu);
                view.setLayoutParams(new FrameLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
                linearLayout.addView(view);
                toolbar.setVisibility(View.GONE);
                Toolbar toolbar1 = (Toolbar) linearLayout.findViewById(R.id.toolbar);
                toolbar1.setNavigationIcon(R.drawable.ic_back);
                toolbar1.setNavigationOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        linearLayout.removeAllViews();
                        toolbar.setVisibility(View.VISIBLE);
                    }
                });
            }
        });

        navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                if (previousMenuItem != null) {
                    previousMenuItem.setChecked(false);
                }
                item.setCheckable(true);
                item.setChecked(true);
                previousMenuItem = item;
                drawerLayout.closeDrawers();

                FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
                switch (item.getItemId()) {
                    case R.id.home:
                        startActivity(new Intent(DashboardActivity.this, DashboardActivity.class));
                        return true;
                    /*case R.id.complaints:
                        startActivity(new Intent(DashboardActivity.this, ComplaintActivity.class));
                        return true;*/
                    case R.id.complaintStatus:
                        startActivity(new Intent(DashboardActivity.this, ComplaintStatusActivity.class));
                        return true;
                    case R.id.about:
                        startActivity(new Intent(DashboardActivity.this, AboutUsActivity.class));
                        return true;
                    case R.id.terms:
                        startActivity(new Intent(DashboardActivity.this, TermsAndConditionsActivity.class));
                        return true;
                    case R.id.logout:
                        new MaterialDialog.Builder(DashboardActivity.this)
                                .title("Logout")
                                .content("Are you sure to Logout?")
                                .positiveText("Yes")
                                .negativeText("Cancel")
                                .onPositive(new MaterialDialog.SingleButtonCallback() {
                                    @Override
                                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                                        startActivity(new Intent(DashboardActivity.this, LoginActivity.class));
                                        ActivityCompat.finishAffinity(DashboardActivity.this);
                                    }
                                })
                                .onNegative(new MaterialDialog.SingleButtonCallback() {
                                    @Override
                                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                                        dialog.dismiss();
                                    }
                                })
                                .typeface("Whitney-Semibold-Bas.otf", "Whitney-Semibold-Bas.otf")
                                .show();
                        return true;
                    default:
                        return true;
                }
            }
        });

        DashboardFragment fragment = new DashboardFragment();
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.frame, fragment);
        transaction.commit();

        ActionBarDrawerToggle actionBarDrawerToggle = new ActionBarDrawerToggle(this, drawerLayout, R.string.openDrawer, R.string.closeDrawer) {
            @Override
            public void onDrawerClosed(View drawerView) {
                super.onDrawerClosed(drawerView);
            }

            @Override
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
            }
        };

        drawerLayout.setDrawerListener(actionBarDrawerToggle);
        actionBarDrawerToggle.syncState();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_dashboard, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case android.R.id.home:
                drawerLayout.openDrawer(GravityCompat.START);
                break;

            case R.id.area:
                startActivity(new Intent(this, AreaActivity.class));
        }
        return super.onOptionsItemSelected(item);
    }
}

