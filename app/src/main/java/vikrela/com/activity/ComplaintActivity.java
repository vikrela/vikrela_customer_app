package vikrela.com.activity;

import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Base64;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.StringTokenizer;

import butterknife.Bind;
import butterknife.ButterKnife;
import vikrela.com.R;
import vikrela.com.app.AppController;
import vikrela.com.app.GPSTracker;
import vikrela.com.constant.NetworkConstants;
import vikrela.com.ui.Snackbar;

/**
 * Created by Harsh on 6/15/2017.
 */
public class ComplaintActivity extends AppCompatActivity {

    @Bind(R.id.toolbar)
    Toolbar toolbar;
    @Bind(R.id.txtCaptureImage)
    TextView txtCaptureImage;
    @Bind(R.id.txtCaptureVideo)
    TextView txtCaptureVideo;
    @Bind(R.id.etLandmark)
    EditText etLandmark;
    @Bind(R.id.etLocation)
    EditText etLocation;
    @Bind(R.id.etStreetAddress)
    EditText etStreetAddress;
    @Bind(R.id.etPincode)
    EditText etPincode;
    @Bind(R.id.spinnerDivision)
    Spinner spinnerDivision;
    @Bind(R.id.etComplaintTitle)
    EditText etComplaintTitle;
    @Bind(R.id.etComplaintDesc)
    EditText etComplaintDesc;
    @Bind(R.id.btnSubmitComplaint)
    Button btnSubmitComplaint;
    @Bind(R.id.imgScanQrBig)
    ImageView imgScanQrBig;
    @Bind(R.id.etHawkerComplaintTitle)
    EditText etHawkerComplaintTitle;
    @Bind(R.id.etHawkerComplaintDesc)
    EditText etHawkerComplaintdesc;
    @Bind(R.id.btnLodgeComplaint)
    Button btnLodgeComplaint;
    @Bind(R.id.llGeneralComplaint)
    LinearLayout llGeneralComplaint;
    @Bind(R.id.llHawkerComplaint)
    LinearLayout llHawkerComplaint;
    @Bind(R.id.radioGroupType)
    RadioGroup radioGroupType;
    @Bind(R.id.radioButtonGeneral)
    RadioButton radioButtonGeneral;
    @Bind(R.id.radioButtonHawker)
    RadioButton radioButtonHawker;
    Bitmap bitmap;
    String id;
    private static final String TAG = ComplaintActivity.class.getSimpleName();
    private Uri fileUri;
    private static final int CAMERA_CAPTURE_IMAGE_REQUEST_CODE = 100;
    private static final int CAMERA_CAPTURE_VIDEO_REQUEST_CODE = 200;
    public static final int MEDIA_TYPE_IMAGE = 1;
    public static final int MEDIA_TYPE_VIDEO = 2;
    GPSTracker tracker;
    double lat, lon;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_complaints);
        populate();
    }

    private void populate() {
        ButterKnife.bind(this);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Complaints Form");
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        llHawkerComplaint.setVisibility(View.GONE);
        imgScanQrBig.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ComplaintActivity.this, ScanQrComplaintActivity.class);
                startActivityForResult(intent, 1500);
            }
        });
        radioGroupType.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                if (radioButtonGeneral.isChecked()) {
                    llGeneralComplaint.setVisibility(View.VISIBLE);
                    llHawkerComplaint.setVisibility(View.GONE);
                } else if (radioButtonHawker.isChecked()) {
                    llHawkerComplaint.setVisibility(View.VISIBLE);
                    llGeneralComplaint.setVisibility(View.GONE);
                }
            }
        });
        txtCaptureImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                captureImage();
            }
        });
        txtCaptureVideo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                recordVideo();
            }
        });
        tracker = new GPSTracker(this);
        btnSubmitComplaint.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (etLandmark.getText().toString().equals("")) {
                    Snackbar.show(ComplaintActivity.this, getString(R.string.enter_landmark));
                    return;
                }
                if (etLocation.getText().toString().equals("")) {
                    Snackbar.show(ComplaintActivity.this, getString(R.string.enter_location));
                    return;
                }
                if (etStreetAddress.getText().toString().equals("")) {
                    Snackbar.show(ComplaintActivity.this, getString(R.string.enter_street));
                    return;
                }
                if (etPincode.getText().toString().equals("")) {
                    Snackbar.show(ComplaintActivity.this, getString(R.string.enter_pincode));
                    return;
                }
                if (etPincode.getText().toString().length() != 6) {
                    Snackbar.show(ComplaintActivity.this, getString(R.string.valid_pincode));
                    return;
                }
                if (etComplaintTitle.getText().toString().equals("")) {
                    Snackbar.show(ComplaintActivity.this, getString(R.string.enter_complaint_title));
                    return;
                }
                if (txtCaptureImage.getText().toString().equals("Take Image")) {
                    Snackbar.show(ComplaintActivity.this, getString(R.string.capture_image));
                    return;
                }
                final MaterialDialog dialog = new MaterialDialog.Builder(ComplaintActivity.this)
                        .title("Loading")
                        .content("Please Wait...")
                        .progress(true, 0)
                        .typeface("Whitney-Semibold-Bas.otf", "Whitney-Semibold-Bas.otf")
                        .show();

                if (tracker.canGetLocation()) {
                    lat = tracker.getLatitude();
                    lon = tracker.getLongitude();
                }
                StringRequest request = new StringRequest(Request.Method.POST, NetworkConstants.COMPLAINT_URL, new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        dialog.dismiss();
                        Log.d("Response:::", response);
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            boolean message = jsonObject.getBoolean("message");
                            if (message) {
                                String complaintId = jsonObject.getString("id");
                                new MaterialDialog.Builder(ComplaintActivity.this)
                                        .title("Confirmation")
                                        .content("Your Compaint has been registered.\n Complaint ID : " + complaintId)
                                        .positiveText("Ok")
                                        .typeface("Whitney-Semibold-Bas.otf", "Whitney-Semibold-Bas.otf")
                                        .onPositive(new MaterialDialog.SingleButtonCallback() {
                                            @Override
                                            public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                                                startActivity(new Intent(ComplaintActivity.this, DashboardActivity.class));
                                                ActivityCompat.finishAffinity(ComplaintActivity.this);
                                            }
                                        }).show();
                                //Toast.makeText(getApplicationContext(), "Complaint Submitted!!!", Toast.LENGTH_LONG).show();
                            } else {
                                Toast.makeText(getApplicationContext(), "Some Error Occurred", Toast.LENGTH_SHORT).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        dialog.dismiss();
                        Log.e("Error:::", error.toString());
                    }
                }) {
                    @Override
                    protected Map<String, String> getParams() throws AuthFailureError {
                        Map<String, String> params = new HashMap<String, String>();
                        String image = getStringImage(bitmap);
                        params.put("landmark", etLandmark.getText().toString());
                        params.put("area", etLocation.getText().toString());
                        params.put("streetName", etStreetAddress.getText().toString());
                        params.put("pincode", etPincode.getText().toString());
                        params.put("division", spinnerDivision.getSelectedItem().toString());
                        params.put("complaintTitle", etComplaintTitle.getText().toString());
                        params.put("complaintDescription", etComplaintDesc.getText().toString());
                        params.put("complaintImage", image);
                        params.put("complaintVideo", "");
                        params.put("latitude", String.valueOf(lat));
                        params.put("longitude", String.valueOf(lon));
                        return params;
                    }
                };
                request.setRetryPolicy(new DefaultRetryPolicy(0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                AppController.getInstance().addToRequestQueue(request);
            }
        });
    }

    public String getStringImage(Bitmap bmp) {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bmp.compress(Bitmap.CompressFormat.JPEG, 90, baos);
        byte[] imageBytes = baos.toByteArray();
        String encodedImage = Base64.encodeToString(imageBytes, Base64.DEFAULT);
        return encodedImage;
    }

    private void captureImage() {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
/*
        fileUri = getOutputMediaFileUri(MEDIA_TYPE_IMAGE);

        intent.putExtra(MediaStore.EXTRA_OUTPUT, fileUri);*/

        // start the image capture Intent
        startActivityForResult(intent, CAMERA_CAPTURE_IMAGE_REQUEST_CODE);
    }

    private void recordVideo() {
        Intent intent = new Intent(MediaStore.ACTION_VIDEO_CAPTURE);

        fileUri = getOutputMediaFileUri(MEDIA_TYPE_VIDEO);

        // set video quality
        intent.putExtra(MediaStore.EXTRA_VIDEO_QUALITY, 1);

        intent.putExtra(MediaStore.EXTRA_OUTPUT, fileUri); // set the image file
        // name

        // start the video capture Intent
        startActivityForResult(intent, CAMERA_CAPTURE_VIDEO_REQUEST_CODE);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        // save file url in bundle as it will be null on screen orientation
        // changes
        outState.putParcelable("file_uri", fileUri);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);

        // get the file url
        fileUri = savedInstanceState.getParcelable("file_uri");
    }

    public Uri getOutputMediaFileUri(int type) {
        return Uri.fromFile(getOutputMediaFile(type));
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        // if the result is capturing Image
        if (requestCode == CAMERA_CAPTURE_IMAGE_REQUEST_CODE) {
            if (resultCode == RESULT_OK) {
                bitmap = (Bitmap) data.getExtras().get("data");
                txtCaptureImage.setText("Image Saved");
                txtCaptureImage.setEnabled(false);
                // successfully captured the image
                // launching upload activity
                //launchUploadActivity(true);


            } else if (resultCode == RESULT_CANCELED) {

                // user cancelled Image capture
                Toast.makeText(getApplicationContext(),
                        "User cancelled image capture", Toast.LENGTH_SHORT)
                        .show();

            } else {
                // failed to capture image
                Toast.makeText(getApplicationContext(),
                        "Sorry! Failed to capture image", Toast.LENGTH_SHORT)
                        .show();
            }

        } else if (requestCode == CAMERA_CAPTURE_VIDEO_REQUEST_CODE) {
            if (resultCode == RESULT_OK) {

                // video successfully recorded
                // launching upload activity
                //launchUploadActivity(false);

            } else if (resultCode == RESULT_CANCELED) {

                // user cancelled recording
                Toast.makeText(getApplicationContext(),
                        "User cancelled video recording", Toast.LENGTH_SHORT)
                        .show();

            } else {
                // failed to record video
                Toast.makeText(getApplicationContext(),
                        "Sorry! Failed to record video", Toast.LENGTH_SHORT)
                        .show();
            }
        } else if (requestCode == 1500){
            if (resultCode == RESULT_OK){
                String url = (String) data.getExtras().get("URL");
                if (!url.equals("")) {
                    StringTokenizer str = new StringTokenizer(url, "id=");
                    while (str.hasMoreElements()) {
                        id = str.nextElement().toString();
                    }
                }
                imgScanQrBig.setEnabled(false);
                imgScanQrBig.setImageResource(R.drawable.checked);
            }

        }
    }

    private static File getOutputMediaFile(int type) {

        // External sdcard location
        File mediaStorageDir = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES), "Vikrela");

        // Create the storage directory if it does not exist
        if (!mediaStorageDir.exists()) {
            if (!mediaStorageDir.mkdirs()) {
                Log.d(TAG, "Oops! Failed create "
                        + "Vikrela" + " directory");
                return null;
            }
        }

        // Create a media file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss",
                Locale.getDefault()).format(new Date());
        File mediaFile;
        if (type == MEDIA_TYPE_IMAGE) {
            mediaFile = new File(mediaStorageDir.getPath() + File.separator
                    + "IMG_" + timeStamp + ".jpg");
        } else if (type == MEDIA_TYPE_VIDEO) {
            mediaFile = new File(mediaStorageDir.getPath() + File.separator
                    + "VID_" + timeStamp + ".mp4");
        } else {
            return null;
        }

        return mediaFile;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case android.R.id.home:
                onBackPressed();
        }
        return super.onOptionsItemSelected(item);
    }
}
