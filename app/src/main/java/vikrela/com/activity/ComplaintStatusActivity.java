package vikrela.com.activity;

import android.os.Bundle;
import android.os.PersistableBundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import vikrela.com.R;
import vikrela.com.adapter.ComplaintAdapter;
import vikrela.com.dto.ComplaintDTO;

/**
 * Created by Harsh on 6/19/2017.
 */
public class ComplaintStatusActivity extends AppCompatActivity {

    @Bind(R.id.toolbar)
    Toolbar toolbar;
    @Bind(R.id.complaintList)
    RecyclerView complaintList;
    private List<ComplaintDTO> complaintDTOList;
    private ComplaintAdapter adapter;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_complaint_status);
        populate();
    }

    private void populate() {
        ButterKnife.bind(this);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Complaint Status");
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        adapter = new ComplaintAdapter(ComplaintStatusActivity.this, getData(), new ComplaintAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(ComplaintDTO complaintDTO) {

            }
        });
        complaintList.setAdapter(adapter);
        adapter.notifyDataSetChanged();
        complaintList.setLayoutManager(new LinearLayoutManager(ComplaintStatusActivity.this));
    }

    public List<ComplaintDTO> getData() {
        complaintDTOList = new ArrayList<>();
        String[] title = {"Illegal Hawking", "Rotten Fruit Selling"};
        String[] desc = {"Unknown Hawker talking abusively to customers", "Fruits being sold are all rotten"};
        String[] date = {"16/06/2017", "17/06/2017"};
        String[] status = {"Attended: Complaint Filed", "Pending"};
        for (int i = 0; i < title.length; i++) {
            ComplaintDTO dto = new ComplaintDTO();
            dto.setTitle(title[i]);
            dto.setDesc(desc[i]);
            dto.setDate(date[i]);
            dto.setStatus(status[i]);
            complaintDTOList.add(dto);
        }
        return complaintDTOList;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case android.R.id.home:
                onBackPressed();
                break;
        }
        return super.onOptionsItemSelected(item);
    }
}
