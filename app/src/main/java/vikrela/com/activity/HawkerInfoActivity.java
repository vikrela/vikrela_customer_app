package vikrela.com.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;
import java.util.StringTokenizer;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import vikrela.com.R;
import vikrela.com.app.AppController;
import vikrela.com.app.GPSTracker;
import vikrela.com.constant.NetworkConstants;
import vikrela.com.ui.Snackbar;

/**
 * Created by Harsh on 6/15/2017.
 */
public class HawkerInfoActivity extends AppCompatActivity {
    @Bind(R.id.toolbar)
    Toolbar toolbar;
    @Bind(R.id.btnPing)
    Button btnPing;
    @Bind(R.id.btnRateHawker)
    Button btnRateHawker;
    @Bind(R.id.infoView)
    RelativeLayout infoView;
    @Bind(R.id.cardHawkerInfo)
    CardView cardHawkerInfo;
    String id = new String();
    TextView txtNameVal, txtMobileVal, txtTypeVal, txtPeriod, txtStreetVal, txtAddressVal, txtDOBVal, txtGenderVal, txtSelectCityVal, txtHandicapped, txtCasteVal;
    MaterialDialog dialog;
    GPSTracker tracker;
    double lat = 0, lon = 0;
    String TAG = "HawkerInfo";

    @OnClick(R.id.btnPing)
    void share() {
        Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
        sharingIntent.setType("text/plain");
        String shareBody = txtNameVal.getText().toString() + " in our Society. Let's go buy some veggies.";
        sharingIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, "Subject Here");
        sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, shareBody);
        startActivity(Intent.createChooser(sharingIntent, "Share via"));
    }

    @OnClick(R.id.btnRateHawker)
    void rate() {
        final MaterialDialog dialog = new MaterialDialog.Builder(HawkerInfoActivity.this)
                .customView(R.layout.rating_dialog_custom_view, true)
                .title("Rate Hawker")
                .cancelable(true)
                .show();
        Button btnSubmit = (Button) dialog.findViewById(R.id.btnSubmitRating);
        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                Snackbar.success(HawkerInfoActivity.this, "Rating Submitted!!!");
            }
        });
        btnRateHawker.setEnabled(false);
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_hawker_info);
        ButterKnife.bind(this);
        tracker = new GPSTracker(HawkerInfoActivity.this);
        if (tracker.canGetLocation()) {
            lat = tracker.getLatitude();
            lon = tracker.getLongitude();
        }
        infoView.setVisibility(View.INVISIBLE);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Hawker Information");
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        dialog = new MaterialDialog.Builder(HawkerInfoActivity.this)
                .title("Loading...")
                .content("Fetching Data...")
                .progress(true, 0)
                .show();
        txtNameVal = (TextView) findViewById(R.id.txtNameVal);
        txtMobileVal = (TextView) findViewById(R.id.txtMobileVal);
        txtTypeVal = (TextView) findViewById(R.id.txtTypeVal);
        txtDOBVal = (TextView) findViewById(R.id.txtDOBVal);
        txtGenderVal = (TextView) findViewById(R.id.txtGenderVal);
        txtSelectCityVal = (TextView) findViewById(R.id.txtSelectCityVal);
        txtHandicapped = (TextView) findViewById(R.id.txtHandicappedVal);
        txtCasteVal = (TextView) findViewById(R.id.txtCasteVal);
        txtPeriod = (TextView) findViewById(R.id.txtDOBVal);
        txtStreetVal = (TextView) findViewById(R.id.txtGenderVal);
        txtAddressVal = (TextView) findViewById(R.id.txtSelectCityVal);
        Bundle bundle = getIntent().getExtras();
        if (null != bundle) {
            if (bundle.containsKey("URL")) {
                String url = bundle.getString("URL");
                StringTokenizer str = new StringTokenizer(url, "id=");
                while (str.hasMoreElements()) {
                    id = str.nextElement().toString();
                }
                populate(id);
            }
            if (bundle.containsKey("license")) {
                id = bundle.getString("license");
                viaLicense(id);
            }
        }

        //Toast.makeText(this, id, Toast.LENGTH_LONG).show();

    }

    private void viaLicense(final String id) {
        StringRequest jsonReq = new StringRequest(Request.Method.POST,
                NetworkConstants.INFORMATION_URL_ID, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.d(TAG, "Response: " + response);
                JSONObject jsonObject = null;
                try {
                    jsonObject = new JSONObject(response);
                    parseJsonFeed(jsonObject);
                } catch (JSONException e) {
                    Log.d(TAG, "Catch" + e.getMessage());
                    e.printStackTrace();
                }
            }

        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d(TAG, "Error: " + error.getMessage());
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                //if(bundle!=null)
                params.put("id", id);
                params.put("latitude", String.valueOf(lat));
                params.put("longitude", String.valueOf(lon));
                //else
                //    Log.d("Bundle","Bundle is null");
                return params;
            }
        };
        jsonReq.setRetryPolicy(new DefaultRetryPolicy(0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppController.getInstance().addToRequestQueue(jsonReq);
    }

    private void populate(final String id) {

        StringRequest jsonReq = new StringRequest(Request.Method.POST,
                NetworkConstants.INFORMATION_URL, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.d(TAG, "Response: " + response);
                JSONObject jsonObject = null;
                try {
                    jsonObject = new JSONObject(response);
                    parseJsonFeed(jsonObject);
                } catch (JSONException e) {
                    Log.d(TAG, "Catch" + e.getMessage());
                    e.printStackTrace();
                }
            }

        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d(TAG, "Error: " + error.getMessage());
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                //if(bundle!=null)
                params.put("id", id);
                params.put("latitude", String.valueOf(lat));
                params.put("longitude", String.valueOf(lon));
                //else
                //    Log.d("Bundle","Bundle is null");
                return params;
            }
        };
        jsonReq.setRetryPolicy(new DefaultRetryPolicy(0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppController.getInstance().addToRequestQueue(jsonReq);
    }

    /**
     * Parsing json reponse and passing the data to feed view list adapter
     */
    private void parseJsonFeed(JSONObject response) {
        try {
            JSONArray feedArray = response.getJSONArray("result");
            infoView.setVisibility(View.VISIBLE);
            dialog.dismiss();
            for (int i = 0; i < feedArray.length(); i++) {
                JSONObject feedObj = (JSONObject) feedArray.get(i);
                txtNameVal.setText(feedObj.getString("name"));
                txtMobileVal.setText(feedObj.getString("mobile"));
                txtTypeVal.setText(feedObj.getString("type"));
                txtHandicapped.setText(feedObj.getString("handicap"));
                txtCasteVal.setText(feedObj.getString("caste"));
                txtPeriod.setText(feedObj.getString("period") + " years");
                txtGenderVal.setText(feedObj.getString("street_name"));
                txtAddressVal.setText(feedObj.getString("business_place"));
            }
            if (feedArray.length() == 0) {
                cardHawkerInfo.setVisibility(View.GONE);
                btnPing.setVisibility(View.GONE);
                btnRateHawker.setVisibility(View.GONE);
                new MaterialDialog.Builder(HawkerInfoActivity.this)
                        .title("Alert")
                        .content("Sorry, this hawker is not registered with the NMC.")
                        .positiveText(getString(R.string.ok))
                        .onPositive(new MaterialDialog.SingleButtonCallback() {
                            @Override
                            public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                                dialog.dismiss();
                                onBackPressed();
                                finish();
                            }
                        })
                        .typeface("Whitney-Semibold-Bas.otf", "Whitney-Semibold-Bas.otf")
                        .show();
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case android.R.id.home:
                onBackPressed();
                break;
        }
        return super.onOptionsItemSelected(item);
    }
}
