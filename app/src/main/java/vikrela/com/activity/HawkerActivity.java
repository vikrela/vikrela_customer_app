package vikrela.com.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import vikrela.com.R;
import vikrela.com.adapter.HawkerAdapter;
import vikrela.com.dto.TopHawkerDTO;

public class HawkerActivity extends AppCompatActivity {

    @Bind(R.id.toolbar)
    Toolbar toolbar;
    @Bind(R.id.hawkerList)
    RecyclerView hawkerList;
    private HawkerAdapter adapter;
    List<TopHawkerDTO> topHawkerDTOList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_hawker);
        populate();
    }

    private void populate() {
        ButterKnife.bind(this);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Hawkers");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        adapter = new HawkerAdapter(HawkerActivity.this, getData(), new HawkerAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(TopHawkerDTO topHawkerDTO) {
                String name = topHawkerDTO.getName();
                Bundle bundle = new Bundle();
                if (name.equals("Shubhendu Awasthi")){
                    bundle.putString("profile", "shubhendu.awasthi");
                }
                if (name.equals("Pratik Khanapurkar")){
                    bundle.putString("profile", "pratik.khanapurkar.3");
                }
                if (name.equals("Harsh Deep Singh")){
                    bundle.putString("profile", "harsh18894");
                }
                Intent intent = new Intent(HawkerActivity.this, ProfileActivity.class);
                intent.putExtras(bundle);
                startActivity(intent);
            }
        });
        hawkerList.setAdapter(adapter);
        hawkerList.setLayoutManager(new LinearLayoutManager(HawkerActivity.this));
        adapter.notifyDataSetChanged();
    }

    public List<TopHawkerDTO> getData() {
        topHawkerDTOList = new ArrayList<>();
        String[] name = {"Shubhendu Awasthi", "Pratik Khanapurkar", "Harsh Deep Singh"};
        String[] address = {"Sant Kabir Nagar", "Indiranagar", "Nasik City Centre"};
        int[] iconId = {R.drawable.boy, R.drawable.girl, R.drawable.boy_abc};
        for (int i = 0; i < iconId.length; i++) {
            TopHawkerDTO dto = new TopHawkerDTO();
            dto.setName(name[i]);
            dto.setBusinessPlace(address[i]);
            dto.setImageUrl(iconId[i]);
            topHawkerDTOList.add(dto);
        }
        return topHawkerDTOList;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case android.R.id.home:
                onBackPressed();
                break;
        }
        return super.onOptionsItemSelected(item);
    }
}
