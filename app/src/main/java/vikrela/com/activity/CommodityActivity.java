package vikrela.com.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ListView;
import android.widget.TextView;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import vikrela.com.R;
import vikrela.com.adapter.CommodityAdapter;
import vikrela.com.app.AppController;
import vikrela.com.dto.CommodityDTO;
import vikrela.com.ui.Snackbar;

public class CommodityActivity extends AppCompatActivity {

    @Bind(R.id.toolbar)
    Toolbar toolbar;
    @Bind(R.id.commodityList)
    ListView commodityList;
    @Bind(R.id.txtCity)
    TextView txtCity;
    private CommodityAdapter adapter;
    private List<CommodityDTO> commodityDTOList;
    MaterialDialog dialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_commodity);
        populate();
    }

    private void populate() {
        ButterKnife.bind(this);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Today's Prices");
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        txtCity.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(CommodityActivity.this, DistrictActivity.class);
                commodityList.setVisibility(View.GONE);
                startActivityForResult(intent, 0);
            }
        });
        commodityDTOList = new ArrayList<>();
        adapter = new CommodityAdapter(CommodityActivity.this, commodityDTOList);
        commodityList.setAdapter(adapter);
/*
        commodityList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                new MaterialDialog.Builder(CommodityActivity.this)
                        .title("Information")
                        .content("Please reach out to the cart set-up at the gate. \n Thank You")
                        .typeface("Whitney-Semibold-Bas.otf", "Whitney-Book-Bas.otf")
                        .show();
            }
        });
*/
    }

    /*public List<CommodityDTO> getData() {
        commodityDTOList = new ArrayList<>();
        String[] commodities = {"Tea", "Coffee", "Water", "Cookies", "Potato Chips", "Vada Pav", "Sandwiches"};
        String[] rates = {"2$", "3$", "1$", "5$", "4$", "10$", "15$"};
        int[] icons = {R.drawable.tea, R.drawable.coffee_cup, R.drawable.water, R.drawable.cookies,
                R.drawable.chips, R.drawable.hamburger, R.drawable.sandwich};
        for (int i = 0; i < commodities.length; i++) {
            CommodityDTO dto = new CommodityDTO();
            dto.setCommodityName(commodities[i]);
            dto.setCommodityRate(rates[i]);
            dto.setIconId(icons[i]);
            commodityDTOList.add(dto);
        }
        return commodityDTOList;
    }*/

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == 0 && resultCode == RESULT_OK) {
            String district = (String) data.getExtras().get("district");
            txtCity.setText(district);
            //getPrices(district);
            String URL = "https://data.gov.in/api/datastore/resource.json?resource_id=9ef84268-d588-465a-a308-a864a43d0070&api-key=280a9c101472ef23192f087107bfa26c&filters[district]=\"" + district + "\"";
            dialog = new MaterialDialog.Builder(CommodityActivity.this)
                    .title("Loading")
                    .content("Please Wait...")
                    .progress(true, 0)
                    .show();

            StringRequest request = new StringRequest(Request.Method.GET, URL, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    dialog.dismiss();
                    Log.d("Response:::", response);
                    try {
                        JSONObject jsonObject = new JSONObject(response);
                        JSONArray jsonArray = jsonObject.getJSONArray("records");
                        if (jsonArray.length() > 0) {
                            commodityList.setVisibility(View.VISIBLE);
                            for (int i = 0; i < jsonArray.length(); i++) {
                                JSONObject jsonObject1 = jsonArray.getJSONObject(i);
                                CommodityDTO dto = new CommodityDTO();
                                String min = jsonObject1.getString("min_price");
                                String max = jsonObject1.getString("max_price");
                                String modal = jsonObject1.getString("modal_price");
                                int mini = Integer.parseInt(min) / 100;
                                int maxi = Integer.parseInt(max) / 100;
                                int mod = Integer.parseInt(modal) / 100;
                                dto.setCommodityName(jsonObject1.getString("commodity"));
                                dto.setCommodityMinRate(String.valueOf(mini));
                                dto.setCommodityMaxRate(String.valueOf(maxi));
                                dto.setCommodityModalPrice(String.valueOf(mod));
                                dto.setMarket(jsonObject1.getString("market"));
                                commodityDTOList.add(dto);
                            }
                            adapter.notifyDataSetChanged();
                        } else {
                            new MaterialDialog.Builder(CommodityActivity.this)
                                    .title("Sorry")
                                    .content("Today's Prices are still not set.. :(")
                                    .positiveText("Ok")
                                    .onPositive(new MaterialDialog.SingleButtonCallback() {
                                        @Override
                                        public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                                            onBackPressed();
                                        }
                                    })
                                    .show();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    dialog.dismiss();
                    Log.e("Error:::", error.toString());
                    Snackbar.show(CommodityActivity.this, "Network Error!!!");
                }
            });
            AppController.getInstance().addToRequestQueue(request);
        }
    }

    public void getPrices(String district) {
        String URL = "https://data.gov.in/api/datastore/resource.json?resource_id=9ef84268-d588-465a-a308-a864a43d0070&api-key=280a9c101472ef23192f087107bfa26c&filters[district]=\"" + district + "\"";
        dialog = new MaterialDialog.Builder(CommodityActivity.this)
                .title("Loading")
                .content("Please Wait...")
                .progress(true, 0)
                .show();


        StringRequest request = new StringRequest(Request.Method.GET, URL, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                dialog.dismiss();
                Log.d("Response:::", response);
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    JSONArray jsonArray = jsonObject.getJSONArray("records");
                    if (jsonArray.length() > 0) {
                        commodityList.setVisibility(View.VISIBLE);
                        for (int i = 0; i < jsonArray.length(); i++) {
                            JSONObject jsonObject1 = jsonArray.getJSONObject(i);
                            CommodityDTO dto = new CommodityDTO();
                            String min = jsonObject1.getString("min_price");
                            String max = jsonObject1.getString("max_price");
                            String modal = jsonObject1.getString("modal_price");
                            int mini = Integer.parseInt(min) / 100;
                            int maxi = Integer.parseInt(max) / 100;
                            int mod = Integer.parseInt(modal) / 100;
                            dto.setCommodityName(jsonObject1.getString("commodity"));
                            dto.setCommodityMinRate(String.valueOf(mini));
                            dto.setCommodityMaxRate(String.valueOf(maxi));
                            dto.setCommodityModalPrice(String.valueOf(mod));
                            dto.setMarket(jsonObject1.getString("market"));
                            commodityDTOList.add(dto);
                        }
                        adapter = new CommodityAdapter(CommodityActivity.this, commodityDTOList);
                        commodityList.setAdapter(adapter);

                    } else {
                        new MaterialDialog.Builder(CommodityActivity.this)
                                .title("Sorry")
                                .content("Today's Prices are still not set.. :(")
                                .positiveText("Ok")
                                .onPositive(new MaterialDialog.SingleButtonCallback() {
                                    @Override
                                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                                        onBackPressed();
                                    }
                                })
                                .show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                dialog.dismiss();
                Log.e("Error:::", error.toString());
                Snackbar.show(CommodityActivity.this, "Network Error!!!");
            }
        });
        AppController.getInstance().addToRequestQueue(request);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case android.R.id.home:
                onBackPressed();
                break;
        }
        return super.onOptionsItemSelected(item);
    }
}
