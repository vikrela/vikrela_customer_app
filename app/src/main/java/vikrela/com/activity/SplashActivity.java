package vikrela.com.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;

import butterknife.ButterKnife;
import vikrela.com.R;
import vikrela.com.app.GPSTracker;

/**
 * Created by Harsh on 6/14/2017.
 */
public class SplashActivity extends AppCompatActivity {

    GPSTracker tracker;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        populate();
    }

    private void populate() {
        ButterKnife.bind(this);
        tracker = new GPSTracker(this);
        if (tracker.canGetLocation()) {

        } else {

        }
        Thread thread = new Thread() {
            public void run() {
                try {
                    sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                } finally {
                    startActivity(new Intent(SplashActivity.this, DashboardActivity.class));
                    //startActivity(new Intent(SplashActivity.this, PreferencesActivity.class));
                }
            }
        };
        thread.start();
    }

    @Override
    protected void onPause() {
        super.onPause();
        finish();
    }

    @Override
    public void onBackPressed() {

    }
}
