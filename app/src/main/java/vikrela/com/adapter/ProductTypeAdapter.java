package vikrela.com.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.List;

import vikrela.com.R;
import vikrela.com.dto.ProductTypeDTO;

/**
 * Created by Harsh on 6/14/2017.
 */
public class ProductTypeAdapter extends RecyclerView.Adapter<ProductTypeAdapter.ProductTypeViewHolder> {


    private Context context;
    private List<ProductTypeDTO> productTypeDTOList;
    private LayoutInflater inflater;
    private OnItemClickListener listener;

    public ProductTypeAdapter(Context context, List<ProductTypeDTO> productTypeDTOList, OnItemClickListener listener) {
        this.context = context;
        this.productTypeDTOList = productTypeDTOList;
        this.listener = listener;
        inflater = LayoutInflater.from(context);
    }

    @Override
    public ProductTypeViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.view_product, parent, false);
        ProductTypeViewHolder holder = new ProductTypeViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(ProductTypeViewHolder holder, int position) {
        holder.bind(productTypeDTOList.get(position), listener);
    }

    @Override
    public int getItemCount() {
        return productTypeDTOList.size();
    }

    public interface OnItemClickListener {
        void onItemClick(ProductTypeDTO productTypeDTO);
    }

    class ProductTypeViewHolder extends RecyclerView.ViewHolder {

        TextView txtName;
        ImageView imgIcon;

        public ProductTypeViewHolder(View itemView) {
            super(itemView);

            txtName = (TextView) itemView.findViewById(R.id.txtName);
            imgIcon = (ImageView) itemView.findViewById(R.id.imgIcon);
        }

        public void bind(final ProductTypeDTO productTypeDTO, final OnItemClickListener listener) {
            txtName.setText(productTypeDTO.getName());
            Glide.with(context).load(productTypeDTO.getIcon()).into(imgIcon);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.onItemClick(productTypeDTO);
                }
            });
        }
    }
}
