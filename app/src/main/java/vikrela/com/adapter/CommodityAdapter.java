package vikrela.com.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.List;

import vikrela.com.R;
import vikrela.com.dto.CommodityDTO;

/**
 * Created by Harsh on 6/30/2017.
 */
public class CommodityAdapter extends BaseAdapter {
    private Context context;
    private List<CommodityDTO> commodityDTOList;
    private LayoutInflater inflater;

    public CommodityAdapter(Context context, List<CommodityDTO> commodityDTOList) {
        this.context = context;
        this.commodityDTOList = commodityDTOList;
        inflater = LayoutInflater.from(context);
    }

    @Override
    public int getCount() {
        return commodityDTOList.size();
    }

    @Override
    public Object getItem(int position) {
        return commodityDTOList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        TextView txtCommodityName, txtCommodityMinRate, txtCommodityMaxRate, txtCommodityModalRate, txtMarket;
        if (convertView == null) {
            convertView = inflater.inflate(R.layout.commodity_custom_row, parent, false);
            txtCommodityName = (TextView) convertView.findViewById(R.id.txtCommodityName);
            txtCommodityMinRate = (TextView) convertView.findViewById(R.id.txtCommodityRate);
            txtCommodityMaxRate = (TextView) convertView.findViewById(R.id.txtCommodityMaxRate);
            txtCommodityModalRate = (TextView) convertView.findViewById(R.id.txtCommodityModalRate);
            txtMarket = (TextView) convertView.findViewById(R.id.txtMarketName);
            txtCommodityName.setText(commodityDTOList.get(position).getCommodityName() + " (/Kg)");
            txtCommodityMinRate.setText("Min. Rs. " + commodityDTOList.get(position).getCommodityMinRate());
            txtCommodityMaxRate.setText("Max. Rs. " + commodityDTOList.get(position).getCommodityMaxRate());
            txtCommodityModalRate.setText("Modal Price: Rs. " + commodityDTOList.get(position).getCommodityModalPrice());
            txtMarket.setText("Market : " + commodityDTOList.get(position).getMarket());
        } else {
            convertView.getTag();
        }
        return convertView;
    }
}
