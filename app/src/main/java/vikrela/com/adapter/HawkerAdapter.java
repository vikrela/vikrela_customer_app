package vikrela.com.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.List;

import vikrela.com.R;
import vikrela.com.dto.TopHawkerDTO;

/**
 * Created by Harsh on 6/29/2017.
 */
public class HawkerAdapter extends RecyclerView.Adapter<HawkerAdapter.HawkerViewHolder>{
    private Context context;
    private List<TopHawkerDTO> topHawkerDTOList;
    private LayoutInflater inflater;
    private OnItemClickListener listener;

    public HawkerAdapter(Context context, List<TopHawkerDTO> topHawkerDTOList, OnItemClickListener listener) {
        this.context = context;
        this.topHawkerDTOList = topHawkerDTOList;
        this.listener = listener;
        inflater = LayoutInflater.from(context);
    }

    @Override
    public HawkerViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.hawker_list_single_row, parent, false);
        HawkerViewHolder holder = new HawkerViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(HawkerViewHolder holder, int position) {
        holder.bind(topHawkerDTOList.get(position), listener);
    }

    @Override
    public int getItemCount() {
        return topHawkerDTOList.size();
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    public interface OnItemClickListener {
        void onItemClick(TopHawkerDTO topHawkerDTO);
    }

    class HawkerViewHolder extends RecyclerView.ViewHolder {
        ImageView imgProfilePic;
        TextView txtName, txtBusinessPlace, txtRatings, txtRate, txtRatingsText;
        RatingBar ratingBar;

        public HawkerViewHolder(View itemView) {
            super(itemView);
            txtName = (TextView) itemView.findViewById(R.id.txtName);
            txtBusinessPlace = (TextView) itemView.findViewById(R.id.txtStreet);
            imgProfilePic = (ImageView) itemView.findViewById(R.id.imgProfilePic);
            ratingBar = (RatingBar) itemView.findViewById(R.id.ratingBar);
            txtRatings = (TextView) itemView.findViewById(R.id.txtRatings);
            txtRate = (TextView) itemView.findViewById(R.id.txtRate);
            txtRatingsText = (TextView) itemView.findViewById(R.id.txtRatingsText);
        }

        public void bind(final TopHawkerDTO topHawkerDTO, final OnItemClickListener listener) {
            txtName.setText(topHawkerDTO.getName());
            txtBusinessPlace.setText(topHawkerDTO.getBusinessPlace());
            ratingBar.setVisibility(View.GONE);
            txtRatingsText.setVisibility(View.GONE);
            ratingBar.setRating(topHawkerDTO.getRating());
            ratingBar.setEnabled(false);
            int pos = getPosition();
            if (pos == 0){
                txtRate.setText("Sells : Street Foods");
            }
            if (pos == 1){
                txtRate.setText("Sells : Flowers");
            }
            if (pos == 2){
                txtRate.setText("Sells : Pan Bidi");
            }
            txtRatings.setVisibility(View.GONE);
            txtRatings.setText(topHawkerDTO.getVotes());
            Glide.with(context).load(topHawkerDTO.getImageUrl()).into(imgProfilePic);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.onItemClick(topHawkerDTO);
                }
            });
        }
    }
}
