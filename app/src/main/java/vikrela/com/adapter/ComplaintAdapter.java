package vikrela.com.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import vikrela.com.R;
import vikrela.com.dto.ComplaintDTO;

/**
 * Created by Harsh on 6/19/2017.
 */
public class ComplaintAdapter extends RecyclerView.Adapter<ComplaintAdapter.ComplaintViewHolder> {
    private LayoutInflater inflater;
    private Context context;
    private List<ComplaintDTO> complaintDTOList;

    public ComplaintAdapter(Context context, List<ComplaintDTO> complaintDTOList, OnItemClickListener listener) {
        this.context = context;
        this.complaintDTOList = complaintDTOList;
        this.listener = listener;
        inflater = LayoutInflater.from(context);
    }

    @Override
    public ComplaintViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.complaint_status_custom_row, parent, false);
        ComplaintViewHolder holder = new ComplaintViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(ComplaintViewHolder holder, int position) {
        holder.bind(complaintDTOList.get(position), listener);
    }

    @Override
    public int getItemCount() {
        return complaintDTOList.size();
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    public interface OnItemClickListener {
        void onItemClick(ComplaintDTO complaintDTO);
    }

    private OnItemClickListener listener;

    class ComplaintViewHolder extends RecyclerView.ViewHolder {
        TextView txtComplaintName, txtComplaintDesc, txtComplaintDate, txtComplaintStatus;

        public ComplaintViewHolder(View itemView) {
            super(itemView);

            txtComplaintName = (TextView) itemView.findViewById(R.id.txtComplaintTitle);
            txtComplaintDesc = (TextView) itemView.findViewById(R.id.txtComplaintDesc);
            txtComplaintDate = (TextView) itemView.findViewById(R.id.txtComplaintDate);
            txtComplaintStatus = (TextView) itemView.findViewById(R.id.txtComplaintStatus);
        }

        public void bind(final ComplaintDTO complaintDTO, final OnItemClickListener listener) {
            txtComplaintName.setText(complaintDTO.getTitle());
            txtComplaintDesc.setText(complaintDTO.getDesc());
            txtComplaintDate.setText(complaintDTO.getDate());
            txtComplaintStatus.setText(complaintDTO.getStatus());
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.onItemClick(complaintDTO);
                }
            });
        }
    }
}
