package vikrela.com.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import vikrela.com.fragment.MarketFragment;
import vikrela.com.fragment.TopHawkerFragment;

/**
 * Created by Harsh on 6/15/2017.
 */
public class DashboardPagerAdapter extends FragmentStatePagerAdapter {
    public DashboardPagerAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        Fragment fragment = null;
        switch (position) {
            case 0:
                fragment = new MarketFragment();
                break;
            case 1:
                fragment = new TopHawkerFragment();
                break;
        }
        return fragment;
    }

    @Override
    public int getCount() {
        return 2;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        switch (position) {
            case 0:
                return "Market Info";
            case 1:
                return "Top Hawkers";
            default:
                return null;
        }

    }

}

