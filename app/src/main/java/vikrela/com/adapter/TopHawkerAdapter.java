package vikrela.com.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.List;

import vikrela.com.R;
import vikrela.com.dto.TopHawkerDTO;

/**
 * Created by Harsh on 6/16/2017.
 */
public class TopHawkerAdapter extends RecyclerView.Adapter<TopHawkerAdapter.TopHawkerViewHolder> {
    private Context context;
    private List<TopHawkerDTO> topHawkerDTOList;
    private LayoutInflater inflater;
    private OnItemClickListener listener;

    public TopHawkerAdapter(Context context, List<TopHawkerDTO> topHawkerDTOList, OnItemClickListener listener) {
        this.context = context;
        this.topHawkerDTOList = topHawkerDTOList;
        this.listener = listener;
        inflater = LayoutInflater.from(context);
    }

    @Override
    public TopHawkerViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.hawker_list_single_row, parent, false);
        TopHawkerViewHolder holder = new TopHawkerViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(TopHawkerViewHolder holder, int position) {
        holder.bind(topHawkerDTOList.get(position), listener);
    }

    @Override
    public int getItemCount() {
        return topHawkerDTOList.size();
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    public interface OnItemClickListener {
        void onItemClick(TopHawkerDTO topHawkerDTO);
    }

    class TopHawkerViewHolder extends RecyclerView.ViewHolder {
        ImageView imgProfilePic;
        TextView txtName, txtBusinessPlace, txtRatings, txtRate;
        RatingBar ratingBar;

        public TopHawkerViewHolder(View itemView) {
            super(itemView);
            txtName = (TextView) itemView.findViewById(R.id.txtName);
            txtBusinessPlace = (TextView) itemView.findViewById(R.id.txtStreet);
            imgProfilePic = (ImageView) itemView.findViewById(R.id.imgProfilePic);
            ratingBar = (RatingBar) itemView.findViewById(R.id.ratingBar);
            txtRatings = (TextView) itemView.findViewById(R.id.txtRatings);
        }

        public void bind(final TopHawkerDTO topHawkerDTO, final OnItemClickListener listener) {
            txtName.setText(topHawkerDTO.getName());
            txtBusinessPlace.setText(topHawkerDTO.getBusinessPlace());
            ratingBar.setRating(topHawkerDTO.getRating());
            ratingBar.setEnabled(false);
            txtRatings.setText(topHawkerDTO.getVotes());
            Glide.with(context).load(topHawkerDTO.getImageUrl()).into(imgProfilePic);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.onItemClick(topHawkerDTO);
                }
            });
        }
    }
}
