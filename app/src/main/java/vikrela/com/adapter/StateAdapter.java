package vikrela.com.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import vikrela.com.R;
import vikrela.com.dto.StateDTO;

/**
 * Created by Harsh on 7/11/2017.
 */
public class StateAdapter extends BaseAdapter {
    private Context context;
    private List<StateDTO> stateDTOList = null;
    private ArrayList<StateDTO> arrayList;

    public StateAdapter(Context context, List<StateDTO> stateDTOList) {
        this.context = context;
        this.stateDTOList = stateDTOList;
        this.arrayList = new ArrayList<StateDTO>();
        this.arrayList.addAll(stateDTOList);
    }

    @Override
    public int getCount() {
        return stateDTOList.size();
    }

    @Override
    public Object getItem(int position) {
        return stateDTOList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        TextView txtDistrict;
        convertView = LayoutInflater.from(context).inflate(R.layout.test_layout, parent, false);
        txtDistrict = (TextView) convertView.findViewById(R.id.txtReportName);
        txtDistrict.setText(stateDTOList.get(position).getName());
        return convertView;
    }

    public void filter(String charText) {
        charText = charText.toLowerCase(Locale.getDefault());
        stateDTOList.clear();
        if (charText.length() == 0) {
            stateDTOList.addAll(arrayList);
        } else {
            for (StateDTO dto : arrayList) {
                if (dto.getName().toLowerCase(Locale.getDefault()).contains(charText)) {
                    stateDTOList.add(dto);
                }
            }
        }
        notifyDataSetChanged();
    }
}
