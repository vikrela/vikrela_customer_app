package vikrela.com.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.List;

import vikrela.com.R;
import vikrela.com.dto.CityDTO;

/**
 * Created by Harsh on 6/14/2017.
 */
public class CityAdapter extends RecyclerView.Adapter<CityAdapter.CityViewHolder> {
    private Context context;
    private List<CityDTO> cityDTOList;
    private LayoutInflater inflater;
    private OnItemClickListener listener;

    @Override
    public CityViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.popular_cities_custom_row, parent, false);
        CityViewHolder holder = new CityViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(CityViewHolder holder, int position) {
        holder.bind(cityDTOList.get(position), listener);
    }

    @Override
    public int getItemCount() {
        return cityDTOList.size();
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    public interface OnItemClickListener {
        void onItemClick(CityDTO cityDTO);
    }

    public CityAdapter(Context context, List<CityDTO> cityDTOList, OnItemClickListener listener) {
        this.context = context;
        this.cityDTOList = cityDTOList;
        this.listener = listener;
        inflater = LayoutInflater.from(context);
    }

    class CityViewHolder extends RecyclerView.ViewHolder {
        ImageView imgIcon;
        TextView txtName;

        public CityViewHolder(View itemView) {
            super(itemView);
            imgIcon = (ImageView) itemView.findViewById(R.id.imgIcon);
            txtName = (TextView) itemView.findViewById(R.id.txtCityName);
        }


        public void bind(final CityDTO cityDTO, final OnItemClickListener listener) {
            txtName.setText(cityDTO.getName());
            Glide.with(context).load(cityDTO.getIcon()).into(imgIcon);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.onItemClick(cityDTO);
                }
            });
        }
    }
}
