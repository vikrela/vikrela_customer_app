package vikrela.com.fragment;


import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.CardView;
import android.text.InputFilter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import vikrela.com.R;
import vikrela.com.activity.CommodityActivity;
import vikrela.com.activity.ComplaintActivity;
import vikrela.com.activity.HawkerActivity;
import vikrela.com.activity.HawkerInfoActivity;
import vikrela.com.activity.ScanQrCodeActivity;
import vikrela.com.ui.Snackbar;

/**
 * A simple {@link Fragment} subclass.
 */
public class MarketFragment extends Fragment {

    View parentView;
    @Bind(R.id.imgScanQr)
    ImageView imgScanQr;
    @Bind(R.id.cardPrices)
    CardView cardPrices;
    @Bind(R.id.btnSubmitLicense)
    Button btnSubmitLicense;
    @Bind(R.id.etLicenseNumber)
    EditText etLicenseNumber;
    @Bind(R.id.cardKnowHawker)
    CardView cardKnowHawker;
    @Bind(R.id.cardComplaints)
    CardView cardComplaints;
    @Bind(R.id.txtCommodityPrice)
    TextView txtCommodityPrice;
    @Bind(R.id.txtComplaints)
    TextView txtComplaints;
    @Bind(R.id.txtKnowYourHawker)
    TextView txtKnowHawkers;
    @Bind(R.id.txtQues)
    TextView txtQues;
    @Bind(R.id.txtLicenseNumber)
    TextView txtLicenseNumber;
    @Bind(R.id.txtScanQr)
    TextView txtScanQr;

    @OnClick(R.id.btnSubmitLicense)
    void info() {
        if (etLicenseNumber.getText().toString().equals("")) {
            Snackbar.show(getActivity(), "No Field Should be Empty!!!");
            return;
        } else {
            Intent intent = new Intent(getActivity(), HawkerInfoActivity.class);
            Bundle bundle = new Bundle();
            bundle.putString("license", etLicenseNumber.getText().toString());
            intent.putExtras(bundle);
            startActivity(intent);
        }
    }

    public MarketFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        parentView = inflater.inflate(R.layout.fragment_market, container, false);
        ButterKnife.bind(this, parentView);
        etLicenseNumber.setFilters(new InputFilter[]{new InputFilter.AllCaps()});

        // Inflate the layout for this fragment
        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        populate();
        return parentView;
    }

    private void populate() {
        Typeface typeface = Typeface.createFromAsset(getActivity().getAssets(), "fonts/Whitney-Semibold-Bas.otf");
        txtCommodityPrice.setTypeface(typeface);
        txtKnowHawkers.setTypeface(typeface);
        txtComplaints.setTypeface(typeface);
        txtQues.setTypeface(typeface);
        btnSubmitLicense.setTypeface(typeface);
        txtLicenseNumber.setTypeface(Typeface.createFromAsset(getActivity().getAssets(), "fonts/Whitney-Book-Bas.otf"));
        txtScanQr.setTypeface(Typeface.createFromAsset(getActivity().getAssets(), "fonts/Whitney-Book-Bas.otf"));
        imgScanQr.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getActivity(), ScanQrCodeActivity.class));
            }
        });

        cardPrices.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                /*new MaterialDialog.Builder(getActivity())
                        .title("Coming Soon")
                        .content("Please wait while we build our Services")
                        .typeface("Whitney-Semibold-Bas.otf", "Whitney-Semibold-Bas.otf")
                        .show();*/
                startActivity(new Intent(getActivity(), CommodityActivity.class));
            }
        });

        cardKnowHawker.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                /*new MaterialDialog.Builder(getActivity())
                        .title("Coming Soon")
                        .content("Please wait while we build our Services")
                        .typeface("Whitney-Semibold-Bas.otf", "Whitney-Semibold-Bas.otf")
                        .show();*/
                startActivity(new Intent(getActivity(), HawkerActivity.class));
            }
        });

        cardComplaints.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getActivity(), ComplaintActivity.class));
            }
        });
    }

}
