package vikrela.com.fragment;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import vikrela.com.R;
import vikrela.com.adapter.ProductTypeAdapter;
import vikrela.com.dto.ProductTypeDTO;

/**
 * A simple {@link Fragment} subclass.
 */
public class ProductTypeFragment extends Fragment {

    View parentView;
    @Bind(R.id.recyclerView)
    RecyclerView recyclerView;
    @Bind(R.id.toolbar)
    Toolbar toolbar;
    @Bind(R.id.ll1)
    LinearLayout ll1;
    private ProductTypeAdapter adapter;
    private List<ProductTypeDTO> data;

    public ProductTypeFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        parentView = inflater.inflate(R.layout.fragment_product_type, container, false);
        ButterKnife.bind(this, parentView);
        populate();
        return parentView;
    }

    private void populate() {
        toolbar.setNavigationIcon(R.drawable.ic_back);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ll1.removeAllViews();
            }
        });
        adapter = new ProductTypeAdapter(getActivity(), getData(), new ProductTypeAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(ProductTypeDTO productTypeDTO) {

            }
        });

        recyclerView.setAdapter(adapter);
        adapter.notifyDataSetChanged();
        recyclerView.setLayoutManager(new GridLayoutManager(getActivity(), 4));

    }

    public List<ProductTypeDTO> getData() {
        data = new ArrayList<>();
        String[] name = {"Shantaram Jilani Sheikh", "Shubhendu Awasthi", "Pratik Khanapurkar", "Harsh Deep Singh"};
        int[] icon = {R.drawable.ic_abc_bank_icon};
        for (int i = 0; i < name.length; i++) {
            ProductTypeDTO dto = new ProductTypeDTO();
            dto.setName(name[i]);
            dto.setIcon(icon[0]);
            data.add(dto);
        }
        return data;
    }
}
