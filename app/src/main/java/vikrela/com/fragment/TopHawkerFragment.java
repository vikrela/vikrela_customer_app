package vikrela.com.fragment;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.afollestad.materialdialogs.MaterialDialog;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import vikrela.com.R;
import vikrela.com.adapter.TopHawkerAdapter;
import vikrela.com.dto.TopHawkerDTO;
import vikrela.com.ui.Snackbar;

/**
 * A simple {@link Fragment} subclass.
 */
public class TopHawkerFragment extends Fragment {

    @Bind(R.id.hawkerList)
    RecyclerView hawkerList;
    private View parentView;
    private List<TopHawkerDTO> topHawkerDTOList;
    private TopHawkerAdapter adapter;

    public TopHawkerFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        parentView = inflater.inflate(R.layout.fragment_top_hawker, container, false);
        ButterKnife.bind(this, parentView);
        populate();
        // Inflate the layout for this fragment
        return parentView;
    }

    private void populate() {
        adapter = new TopHawkerAdapter(getActivity(), getData(), new TopHawkerAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(final TopHawkerDTO topHawkerDTO) {
                /*final MaterialDialog dialog = new MaterialDialog.Builder(getActivity())
                        .customView(R.layout.rating_dialog_custom_view, true)
                        .title("Rate Hawker")
                        .cancelable(true)
                        .show();
                Button btnSubmit = (Button) dialog.findViewById(R.id.btnSubmitRating);
                btnSubmit.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                        Snackbar.success(getActivity(), "Rating Submitted!!!");
                        topHawkerDTO.setVotes(String.valueOf(Integer.parseInt(topHawkerDTO.getVotes()) + 1));
                    }
                });*/
            }
        });
        hawkerList.setAdapter(adapter);
        adapter.notifyDataSetChanged();
        hawkerList.setLayoutManager(new LinearLayoutManager(getActivity()));
    }

    public List<TopHawkerDTO> getData() {
        topHawkerDTOList = new ArrayList<>();
        String[] name = {"Shubhendu Awasthi", "Pratik Khanapurkar", "Harsh Deep Singh"};
        String[] address = {"Sant Kabir Nagar", "Indiranagar", "Nasik City Centre", "Parab Nagar"};
        int[] iconId = {R.drawable.boy, R.drawable.girl, R.drawable.boy_abc};
        int[] rating = {2, 5, 5};
        String[] votes = {"34", "165", "235"};
        for (int i = 0; i < iconId.length; i++) {
            TopHawkerDTO dto = new TopHawkerDTO();
            dto.setName(name[i]);
            dto.setBusinessPlace(address[i]);
            dto.setImageUrl(iconId[i]);
            dto.setRating(rating[i]);
            dto.setVotes(votes[i]);
            topHawkerDTOList.add(dto);
        }
        return topHawkerDTOList;
    }
}
